# opencm3_usbhid (Rust)

This project implements the mouse-jiggling demo from the libopencm3 project.

This project is based on the cortex-m-quickstart template from japaric (used under the MIT Licence).

This project is licensed under the LGPL-3-or-later licence, much like libopencm3 and libopencm3_sys.

## Requirements

This project, much like libopencm3_sys currently, is only for STM32F1 microcontrollers. Small adaptations may be possible to enable use with other microcontrollers.

This project has been tested with a [Blue Pill](http://wiki.stm32duino.com/index.php?title=Blue_Pill), which is an inexpensive (about £2) STM32F103C8T6 microcontroller board.

Note that you may need to add a resistor in order to fix the USB connection, as detailed in the beginning of [this *One Transistor* article](https://www.onetransistor.eu/2017/11/stm32-bluepill-arduino-ide.html).

I used, as the debugger, an ST-Link v2 which was on-board a relatively inexpensive (~£10 from RS Online) STM32F3 discovery board.

With `openocd` installed, I ran `openocd -f interface/stlink-v2.cfg -f target/stm32f1x.cfg` to start the debugging server and then used `cargo run --release` to flash and run the mouse jiggling demo.

**PLEASE NOTE:** This demo DOES NOT WORK in *debug* mode -- it doesn't enumerate as a USB device. I think this is due to poor optimisation of the busy delay (nop) loop. I have not yet investigated how to do a delay properly -- instead, I have only replicated the original libopencm3 example code.

## Notes

This demo is slightly different to the one in the `libopencm3-examples` repository; the mouse moves in a square instead of side-to-side.

A faithful reproduction of the original can be acquired by changing code comments around in the source code.

This demo is also quite messy. It is unlikely to be an elegant example; but it does show correct functioning of USB HID and provides a starting point for cleaning up.
